#!/bin/bash
# This script generates a full netboot Debian image with KVM
# It uses NFS for shared Home-Directories containing several QCOW2 or RAW images.
# A pyboot-manager is used to choose one image to be started
# For furthermore (runtime) configuration it is possible to use a puppet server
# Copyright 2016 Sascha Winkelhofer

rootfs="rootfs"
arch="amd64"
enable_puppet="false"
puppet_master="puppet"
puppet_conf=""
nfs_server="10.10.0.1"
nfs_homes="/export"
nfs_rootfs_path="/boot/rootfs"
homes="/home"
update="false"
boot_from_nfs="false"
user=""
external_script=""



# Executes command in chroot
function chr()
{
    chroot $rootfs bash -c "$@"
}

# Sets multiline string as file content. usage: chr_ml2file <FileName> '<Multiline String>'
function chr_ml2file()
{
    chr "cat >$1 <<EOL
${@:2}
EOL"
}

# Print usage
usage() {
    echo "Usage: $( basename "$0" ) [options]"
    echo "Options:"
    echo "    -r|--rootfs <path>:          Path to RootFS (default -> ./rootfs)"
    echo "    -a|--architecture <arch>:    Architecure (amd64, i386)"
    echo "    -b|--boot-from-nfs <bool>:   RootFS shall be sourced via NFS (true, defaul -> false)"
    echo "    -N|--nfs-server <ip/hostnam>:NFS-Server IP/Hostname (default -> 10.10.0.1)"
    echo "    -B|--rootfs-nfs-path <path>: NFS-Share containing RootFS (defaults to \$nfs-server:/boot/rootfs)"
    echo "    -n|--nfs-homes <nfsHomes>:   NFS home shares (\$nfs-server:/export)"
    echo "    -f|--home-folder <home>:     home directory to mount nfs to (/home)"
    echo "    -p|--enable-puppet <bool>:   Enable Puppet (true,default -> false), needs -P parameter"
    echo "    -P|--puppet-master <url>:    URL of Puppet master (default -> puppet)"
    echo "    -M|--puppet-conf <url>:      Path to local folder containing puppet modules and manifests"
    echo "    -U|--user <username>:        Add user to image (default -> \"\")"
    echo "    -u|--update <bool>:          Update already bootstrapped image (true,default -> false)"
    echo "    -x|--external-script <file>: Execute external bash script during bootstrap"
    exit 0
}
tmp=$@
# Parse command line arguments
current=""
for arg in "$@"; do
    case "$arg" in
        -h|--help)       shift ; usage     ;;
        -u|--update)     shift ; current=update ;;
        -r|--rootfs)    shift ; current=rootfs ;;
        -a|--arch)          shift ; current=arch   ;;
        -b|--boot-from-nfs)          shift ; current=boot_from_nfs   ;;
        -B|--rootfs-nfs-path)          shift ; current=nfs_rootfs_path   ;;
        -f|--home-folder)    shift ; current=homes ;;
        -N|--nfs-server)    shift ; current=nfs_server ;;
        -n|--nfs-homes)    shift ; current=nfs_homes ;;
        -p|--enable-puppet)           shift ; current=enable_puppet    ;;
        -M|--puppet-conf)           shift ; current=puppet_conf    ;;
        -U|--user)          shift; current=user ;;
        -P|--puppet-master)         shift ; current=puppet_master  ;;
        -x|--externa-script)         shift ; current=external_script  ;;
        --) shift ; break ;;
        -*) echo -e >&2 "$( basename "$0" ): Invalid option \`$arg' found\n" && usage ;;
        *) [ -n "$current" ] && eval "$current='$arg'" && current="" && shift || break ;;
    esac
done
# Script must run as root
if [ "$EUID" -ne 0 ]
then
    echo "Installer must run as user root"
    sudo $0 $tmp
    exit
fi
rootfs_path=`dirname $rootfs`

# Check for deboostrap utility
command -v debootstrap >/dev/null 2>&1 || { apt-get install -y debootstrap; }
# Check for pv utility
command -v pv >/dev/null 2>&1 || { apt-get install -y pv; }

if [ ! -d "$rootfs" ]
then
    echo "Bootstrapping root filesystem"
    debootstrap --arch=$arch jessie $rootfs | pv --progress --size 25k > /dev/null
elif [ -d "$rootfs" ] && [ "$update" != "true" ]
then
    echo "Image already bootstrapped, if you want to update set --update command line flag"
    exit 0
fi


# Adjust /etc/network/interfaces
echo "Creating /etc/network/interfaces"
chr_ml2file /etc/network/interfaces 'source-directory /etc/network/interfaces.d
auto lo
iface lo inet loopback
auto eth0
iface eth0 inet dhcp'


# Create /etc/fstab
echo "Creating /etc/fstab with NFS mountpoint $nfs_server:$nfs_homes -> /home"
chr "mkdir -p $homes"
chr_ml2file /etc/fstab "# <file system>   <mount point>   <type>   <options>   <dump>    <pass>
$nfs_server:$nfs_root_path         /              nfs                   0       0
sysfs           /sys            sysfs   defaults,umask=000,gid=108      0       0
proc       /proc      proc   defaults    0   1
tmpfs      /tmp       tmpfs  defaults    0   1
$nfs_server:$nfs_homes  $homes   nfs defaults,nolock   0   0"

# Creating user if necessary
if [ ! -z "$user" ]
then
    echo "Creating user $user:"
    chr "yes Y | adduser $user --home /home/$user -q --disabled-password >/dev/null 2>&1"
    # chr "yes Y | adduser $user --home $homes -q --disabled-password >/dev/null 2>&1"
    chr "passwd $user"
fi

# Writing group permissions to /etc/security/group.conf
echo "Writing group permissions to /etc/security/group.conf"
chr_ml2file /etc/security/group.conf "*;*;*;Al0000-2400;plugdev,Debian-exim,bin,disk,kmem,audio,cdrom,dialout,floppy,libvirt,kvm,libvirt-qemu,sudo,adm,root,sys"

echo "Updating apt..."
chr 'apt-get update 2>&1 > /dev/null'

echo "Installing kernel and modules..."
if [ "$arch" == "i386" ]
then
    chr "DEBIAN_FRONTEND=noninteractive apt-get install -y initramfs-tools >/dev/null 2>&1"
    chr "DEBIAN_FRONTEND=noninteractive apt-get install -y -f >/dev/null 2>&1"
    chr "wget http://kernel.ubuntu.com/~kernel-ppa/mainline/v4.2-wily/linux-headers-4.2.0-040200_4.2.0-040200.201510260713_all.deb -O linux-headers-4.2.0-040200_4.2.0-040200.201510260713_all.deb -q > /dev/null 2>&1"
   chr "dpkg -i linux-headers-4.2.0-040200_4.2.0-040200.201510260713_all.deb >/dev/null 2>&1"
   chr "wget http://kernel.ubuntu.com/~kernel-ppa/mainline/v4.2-wily/linux-image-4.2.0-040200-generic_4.2.0-040200.201510260713_i386.deb -O linux-image-4.2.0-040200-generic_4.2.0-040200.201510260713_i386.deb -q > /dev/null 2>&1"
   chr "dpkg -i linux-image-4.2.0-040200-generic_4.2.0-040200.201510260713_i386.deb >/dev/null 2>&1"
elif [ "$arch" == "amd64" ]
then
    chr "DEBIAN_FRONTEND=noninteractive apt-get install -y initramfs-tools >/dev/null 2>&1"
    chr "DEBIAN_FRONTEND=noninteractive apt-get install -y -f >/dev/null 2>&1"
    chr "wget http://kernel.ubuntu.com/~kernel-ppa/mainline/v4.2-wily/linux-headers-4.2.0-040200_4.2.0-040200.201510260713_all.deb -O linux-headers-4.2.0-040200_4.2.0-040200.201510260713_all.deb -q > /dev/null 2>&1"
   chr "dpkg -i linux-headers-4.2.0-040200_4.2.0-040200.201510260713_all.deb >/dev/null 2>&1"
   chr "wget http://kernel.ubuntu.com/~kernel-ppa/mainline/v4.2-wily/linux-image-4.2.0-040200-generic_4.2.0-040200.201510260713_amd64.deb -O linux-image-4.2.0-040200-generic_4.2.0-040200.201510260713_amd64.deb -q > /dev/null 2>&1"
   chr "dpkg -i linux-image-4.2.0-040200-generic_4.2.0-040200.201510260713_amd64.deb >/dev/null 2>&1"

else
    echo "Architecture failure: $arch not found"
    exit 1
fi

echo "Installing basic packages..."
if [ "$arch" == "i386" ]
then
    chr 'DEBIAN_FRONTEND=noninteractive apt-get install -f -y 2>/dev/null' >/dev/null
    chr 'DEBIAN_FRONTEND=noninteractive apt-get install -y libspice-protocol-dev libspice-server-dev x11-xserver-utils unclutter xinput x11-utils console-data sudo-ldap makedev nfs-common bzip2 xserver-xorg xinit libglib2.0-0 qemu-kvm libvirt0 libvirt-bin pciutils git 2>/dev/null' | pv --progress --size 111k > /dev/null
elif [ "$arch" == "amd64" ]
then
    chr 'DEBIAN_FRONTEND=noninteractive apt-get install -y libspice-protocol-dev libspice-server-dev x11-xserver-utils unclutter xinput x11-utils console-data sudo-ldap makedev nfs-common bzip2 xserver-xorg xinit libglib2.0-0 qemu-kvm libvirt0 libvirt-bin pciutils git' # 2>/dev/null' | pv --progress --size 111k > /dev/null
fi

# Write Module parameters
echo "Setting vfio module parameters"
chr_ml2file /etc/modprobe.d/vfio_iommu_type1.conf "options vfio_iommu_type1 allow_unsafe_interrups=1"

# Set sudo defaults
echo "Setting sudo default options"
chr_ml2file /etc/sudoers 'Defaults        env_keep +="DISPLAY"
Defaults        env_keep +="XAUTHORITY"
Defaults        mail_badpass
Defaults        secure_path="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
root    ALL=(ALL:ALL) ALL

# Allow members of group sudo to execute any command
%sudo   ALL=(ALL:ALL) NOPASSWD:ALL
%admin  ALL=(ALL) NOPASSWD:ALL'


echo "Setting X options"
# Disable energy options
chr "sed -i 's:30:0:g' /etc/kbd/config"
# Modifying global xinitrc
chr_ml2file /etc/X11/xinit/xinitrc 'xset s off
xset -dpms
xset s noblank
xhost +local:root
chmod +s /usr/local/bin/qemu-system
python /opt/pyboot/main.py'

chr "rm -rf /dev/*"

# Use Puppet?

if [ "$enable_puppet" == "true" ]
then
    echo "Installing puppet..."
    chr 'DEBIAN_FRONTEND=noninteractive apt-get install -y puppet > /dev/null 2>&1'
    
    echo "Writing puppet.conf"
    # Setup /etc/puppet/puppet.conf
    chr_ml2file /etc/puppet/puppet.conf "[main]
    logdir=/var/log/puppet
    vardir=/var/lib/puppet
    ssldir=/var/lib/puppet/ssl
    rundir=/var/run/puppet
    factpath=\$vardir/lib/facter
    prerun_command=/etc/puppet/etckeeper-commit-pre
    postrun_command=/etc/puppet/etckeeper-commit-post
    server=$puppet_master
    
    [master]
    # These are needed when the puppetmaster is run by passenger
    # and can safely be removed if webrick is used.
    ssl_client_header = SSL_CLIENT_S_DN 
    ssl_client_verify_header = SSL_CLIENT_VERIFY"
    if  [ ! -z "$puppet_conf" ]
    then
        chr "mknod /dev/random c 1 9"
        chr "mknod /dev/urandom c 1 9"        
        chr "mount /proc"
        cp -r $puppet_conf/* $rootfs/etc/puppet/
        chr "puppet apply --modulepath=/etc/puppet/modules/ /etc/puppet/manifests/init.pp"
        chr "rm -rf /dev/*"
        chr "umount /proc"
    else
        puppet agent -t
        echo "Please press enter after you signed the puppet certificate"
        read nonsense
        puppet agent --enable
        puppet agent -t
    fi
fi

# startx after login
echo "Setting login behavior"
chr_ml2file /etc/profile "startx"

# raise ulimits
echo "Raising ulimits"
chr_ml2file /etc/security/limits.conf "* soft memlock 20000000
* hard memlock 20000000"

# Execute MAKEDEV command on boot, in order to get devices
echo "Installing startup scripts"
chr_ml2file /etc/rc.local '#!/bin/sh -e
# rc.local
#
# This script is executed at the end of each multiuser runlevel.
# Make sure that the script will "exit 0" on success or any other
# value on error.
#
# In order to enable or disable this script just change the execution
# bits.
#
# By default this script does nothing.
/sbin/MAKEDEV
(cd /opt/pyboot && git pull origin master)
loadkeys de
exit 0'

# /etc/rc.local must be executable
chr 'chmod a+x /etc/rc.local'

#Install qemu
echo "Downloading qemu source..."
# download=`curl http://wiki.qemu.org/Download -s 2>/dev/null | grep wikitable -A 7 | grep href | cut -d'"' -f 2`
# chr "cd /root; wget $download -O qemu.tar.bz2 -q > /dev/null 2>&1; tar xjf qemu.tar.bz2; rm qemu.tar.bz2; mv qemu-* qemu"
echo "Cloning qemu..."
chr "cd /root; git clone git://git.qemu-project.org/qemu.git;" >/dev/null

echo "Temporarily install compiler suite..."
packages=`chr "DEBIAN_FRONTEND=noninteractive apt-get install -y gcc make pkg-config libsdl1.2-dev zlib1g-dev libglib2.0-dev autoconf libtool 2>/dev/null" | grep NEW -A 5 | grep -n "^ " | sed 's:$: :g' | tr -d '\n' | sed 's:[0-9]*\:::g'`

echo "Building and installing qemu"
if [ "$arch" == "amd64" ]
then
    chr "cd /root/qemu; ln -s trace-events trace-events-all; ./configure --enable-sdl --enable-spice --target-list=x86_64-softmmu > /dev/null; make -j8 > /dev/null; make install > /dev/null" 2>/dev/null
    chr_ml2file /usr/bin/kvm '#!/bin/sh
    /usr/local/bin/qemu-system-x86_64 -enable-kvm "$@"'

    chr "chmod +x /usr/bin/kvm"
elif [ "$arch" == "i386" ]
then
    chr "cd /root/qemu; ln -s trace-events trace-events-all; ./configure --enable-sdl --enable-spice --target-list=i386-softmmu > /dev/null; make -j8 > /dev/null; make install > /dev/null" 2>/dev/null
fi


# Clean up
chr "rm -rf /root/qemu"

echo "Clean up compiler suite..."
chr "apt-get remove -y $packages > /dev/null 2>&1"

if [ ! -d "$rootfs/opt/pyboot" ]
then
    echo "Cloning pyboot..."
    chr "git clone https://gitlab.com/swinkelhofer/pyboot.git /opt/pyboot" >/dev/null
    chr "apt-get update" >/dev/null
    echo "Installing packages needed for pyboot..."
    chr "DEBIAN_FRONTEND=noninteractive apt-get install -y python-gtk2-dev python-gobject-dev python-webkit-dev libwebkit-dev libwebkitgtk-3.0-dev python-pip" >/dev/null 2>&1
    chr "pip install -r /opt/pyboot/requirements.txt >/dev/null 2>&1"
    chr "apt-get remove -y python-pip"
    chr "apt-get autoremove -y"
else
    chr "cd /opt/pyboot; git pull origin master"
fi

# TODO: Pyboot

echo "Cleaning up apt caches..."
chr "apt-get remove -y make gcc python-pip xz-utils rsync openssh-client wget vim.tiny lvm2 2>&1" >/dev/null
chr "rm /*.deb -rf"
chr "apt-get autoremove -y 2>&1" >/dev/null
chr "apt-get clean"

rm -rf $rootfs_path/tftpboot >/dev/null 2>&1
mkdir $rootfs_path/tftpboot >/dev/null 2>&1

echo "Get pxelinux.0"
if [ "$arch" == "i386" ]
then
   wget http://ftp.nl.debian.org/debian/dists/jessie/main/installer-i386/current/images/netboot/pxelinux.0 -q -O $rootfs_path/tftpboot/pxelinux.0 >/dev/null 2>&1 
    wget ftp://debian.balt.net/debian/dists/jessie/main/installer-i386/20150422/images/netboot/gtk/debian-installer/i386/boot-screens/ldlinux.c32 -O $rootfs_path/tftpboot/ldlinux.c32 -q >/dev/null 2>&1
elif [ "$arch" == "amd64" ]
then
    wget http://ftp.nl.debian.org/debian/dists/jessie/main/installer-amd64/current/images/netboot/pxelinux.0 -q -O $rootfs_path/tftpboot/pxelinux.0 >/dev/null 2>&1
    wget ftp://debian.balt.net/debian/dists/jessie/main/installer-amd64/20150422/images/netboot/gtk/debian-installer/amd64/boot-screens/ldlinux.c32 -O $rootfs_path/tftpboot/ldlinux.c32 -q >/dev/null 2>&1
fi


if [ ! -z "$external_script" ]
then
    chr "bash -c '`cat $external_script`'"
fi

chr_ml2file /etc/initramfs-tools/initramfs.conf "BOOT=nfs

    MODULES=netboot

    BUSYBOX=y
    KEYMAP=n
    COMPRESS=gzip"
chr "update-initramfs -u -t"

echo "Writing PXE boot config"
mkdir $rootfs_path/tftpboot/pxelinux.cfg >/dev/null 2>&1

cat >$rootfs_path/tftpboot/pxelinux.cfg/default <<EOL
default virtual
label virtual
        menu label ^Virtual
        menu default
        kernel /boot/linux
        append initrd=/boot/initrd.gz nfsroot=$nfs_server:$nfs_rootfs_path intel_iommu=on vfio_iommu_type1.allow_unsafe_interrupts=1 intel_iommu=on # vfio_iommu_type1.allow_unsafe_interrupts=1 i915.enable_hd_vgaarb=1 kvm.allow_unsafe_assigned_interrupts=1 kvm-intel.nested=1"
EOL

mkdir $rootfs_path/tftpboot/boot

if [ "$boot_from_nfs" == "true" ]
then
    cp `readlink -f $rootfs/vmlinuz` $rootfs_path/tftpboot/boot/Debian-Jessie-$arch-vmlinuz
    cp `readlink -f $rootfs/initrd.img` $rootfs_path/tftpboot/boot/Debian-Jessie-$arch-initrd.img
    chmod 664 $rootfs_path/tftpboot/boot/Debian-Jessie-$arch-vmlinuz
    chmod 664 $rootfs_path/tftpboot/boot/Debian-Jessie-$arch-initrd.img
    echo "Setting qemu-kvm capabilities"
    setcap cap_dac_override,cap_sys_rawio,cap_sys_admin+eip $rootfs/usr/bin/qemu-system-i386 2>&1 > /dev/null
    setcap cap_dac_override,cap_sys_rawio,cap_sys_admin+eip $rootfs/usr/local/bin/qemu-system-i386 2>&1 > /dev/null
    setcap cap_dac_override,cap_sys_rawio,cap_sys_admin+eip $rootfs/usr/bin/qemu-system-x86_64 2>&1 > /dev/null
    setcap cap_dac_override,cap_sys_rawio,cap_sys_admin+eip $rootfs/usr/local/bin/qemu-system-x86_64 2>&1 > /dev/null
    setcap cap_dac_override,cap_sys_rawio,cap_sys_admin+eip $rootfs/usr/bin/kvm 2>&1 > /dev/null
    setcap cap_dac_override,cap_sys_rawio,cap_sys_admin+eip $rootfs/usr/local/bin/kvm
    echo "Writing PXE boot config"
    mkdir $rootfs_path/tftpboot/pxelinux.cfg >/dev/null 2>&1

    cat >$rootfs_path/tftpboot/pxelinux.cfg/default << EOL
    default virtual
    label virtual
            menu label ^Virtual
            menu default
            kernel /boot/Debian-Jessie-$arch-vmlinuz
            append initrd=/boot/Debian-Jessie-$arch-initrd.img console=tty0 console=tty1 nfs_root=$nfs_server:$nfs_rootfs_path ramdisk_size=160000000 intel_iommu=on vfio_iommu_type1.allow_unsafe_interrupts=1 intel_iommu=on # vfio_iommu_type1.allow_unsafe_interrupts=1 i915.enable_hd_vgaarb=1 kvm.allow_unsafe_assigned_interrupts=1 kvm-intel.nested=1
EOL

    echo "#####################################################################"
    echo "#                       TFTP, DHCP and NFS                          #"
    echo "#####################################################################"
    echo ""
    echo " * Let your DHCP-Server point to your TFTP Server for PXE boot"
    echo " * Copy all the files inside the tftpboot folder to /var/lib/tftpboot"
    echo " * Copy the $rootfs-Folder to your nfs-server"
    echo " * Make sure you have some shares on your NFS server containing VMs"
    echo " * Much fun!"
    exit 0
else
    cp `readlink -f $rootfs/vmlinuz` $rootfs_path/tftpboot/boot/linux
    chmod 664 $rootfs_path/tftpboot/boot/linux
fi

echo "Generating empty image"
dd if=/dev/zero bs=1G count=2 2>/dev/null | pv --size 2100m --progress > initrd

echo "Format image"
mkfs.ext2 initrd >/dev/null 2>&1

mkdir tmp
mount -t ext2 initrd tmp  >/dev/null 2>&1

echo "Copying files to image"
cp -ap $rootfs/* tmp/
echo "Setting qemu-kvm capabilities"
setcap cap_dac_override,cap_sys_rawio,cap_sys_admin+eip tmp/usr/bin/qemu-system-i386 2>&1 > /dev/null
setcap cap_dac_override,cap_sys_rawio,cap_sys_admin+eip tmp/usr/local/bin/qemu-system-i386 2>&1 > /dev/null
setcap cap_dac_override,cap_sys_rawio,cap_sys_admin+eip tmp/usr/bin/qemu-system-x86_64 2>&1 > /dev/null
setcap cap_dac_override,cap_sys_rawio,cap_sys_admin+eip tmp/usr/local/bin/qemu-system-x86_64 2>&1 > /dev/null
setcap cap_dac_override,cap_sys_rawio,cap_sys_admin+eip tmp/usr/bin/kvm 2>&1 > /dev/null
setcap cap_dac_override,cap_sys_rawio,cap_sys_admin+eip tmp/usr/local/bin/kvm 2>&1 > /dev/null
umount tmp
rm -r tmp
echo "GZIPing image"
gzip --stdout -f initrd | pv --progress -s 564m > $rootfs_path/tftpboot/boot/initrd.gz
chmod 664 $rootfs_path/tftpboot/boot/initrd.gz
rm initrd


echo "#####################################################################"
echo "#                       TFTP, DHCP and NFS                          #"
echo "#####################################################################"
echo ""
echo " * Let your DHCP-Server point to your TFTP Server for PXE boot"
echo " * Copy all the files inside the tftpboot folder to /var/lib/tftpboot"
echo " * Make sure you have some shares on your NFS server containing VMs"
echo " * Much fun!"
