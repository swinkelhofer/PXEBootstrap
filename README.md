# Bootstrap PXE Netboot virtualization image

 * Generates a Debian netboot image with KVM to run with PXE 
 * It uses NFS for shared Home-Directories containing several QCOW2 or RAW images that felt to be the actual running OS.
 * A pyboot-manager is used to choose one image to be started
 * For furthermore (runtime) configuration it is possible to use a puppet server

## Usage
```
Usage: install.sh [options]
Options:
    -r|--rootfs <path>:          Path to RootFS (default -> ./rootfs)
    -a|--architecture <arch>:    Architecure (amd64, i386)
    -b|--boot-from-nfs <bool>:   RootFS shall be sourced via NFS (true, defaul -> false)
    -N|--nfs-server <ip/hostnam>:NFS-Server IP/Hostname (default -> 10.10.0.1)
    -B|--rootfs-nfs-path <path>: NFS-Share containing RootFS (defaults to $nfs-server:/boot/rootfs)
    -n|--nfs-homes <nfsHomes>:   NFS home shares ($nfs-server:/export)
    -f|--home-folder <home>:     home directory to mount nfs to (/home)
    -p|--enable-puppet <bool>:   Enable Puppet (true,default -> false), needs -P parameter
    -P|--puppet-master <url>:    URL of Puppet master (default -> puppet)
    -M|--puppet-conf <url>:      Path to local folder containing puppet modules and manifests
    -U|--user <username>:        Add user to image (default -> "")
    -u|--update <bool>:          Update already bootstrapped image (true,default -> false)
    -x|--external-script <file>: Execute external bash script during bootstrap
```
